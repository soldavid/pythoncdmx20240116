---
theme: seriph
layout: cover
background: './python_background.jpg'
info: 2024, David Sol, Public Domain, please reproduce, modify and share
author: David Sol
keywords: python, virtual environments, meetup, CDMX
download: true
exportFilename: 'PythonCDMX20240116'
export:
  format: pdf
  timeout: 30000
  dark: false
  withClicks: false
  withToc: false
selecteable: true
favicon: './python-sticker.png'
fonts:
  sans: 'Ubuntu'
  # use with `font-serif` css class from windicss
  serif: 'Robot Slab'
  mono: 'JetBrains Mono'
themeConfig:
  primary: '#ff8000'
defaults:
  info: 2024, David Sol, Public Domain, please reproduce, modify and share
---

[![Meetup Hero](/hero.jpg)](https://www.meetup.com/python-mexico/events/298328457)

---
layout: image-left
image: './DavidSol.jpg'
---

# David Sol

<br />

## SRE @ Wizeline

<br />
<br />
<br />

### <logos-linkedin-icon /> &nbsp;&nbsp;&nbsp;&nbsp; LinkedIn &nbsp;&nbsp;&nbsp;&nbsp; [soldavidcloud](https://www.linkedin.com/in/soldavidcloud/)

<br />
<br />

### <logos-twitter /> &nbsp;&nbsp;&nbsp;&nbsp; Twitter &nbsp;&nbsp;&nbsp;&nbsp; [@soldavidcloud](https://twitter.com/soldavidcloud)

---
layout: image-right
image: './lightbulb.jpg'
---
# Contenido

<br />

- ¿Qué problema queremos resolver?
  - Versión de Python
  - Versión de Librerias
    - Versionado Semántico
- venv
- poetry
- conda
- pyenv
- otros

---
layout: cover
background: './python_background.jpg'
---

# ¿Qué problema queremos resolver?

---
layout: image
image: './python_background.jpg'
---

# ¿Qué problema queremos resolver?

## Varias Versiones de Python

<br />

* Python del Sistema

<br />

* Nuevas versiones
  * 3.9: Time Zones Support
  * 3.10: Better Error Messages
  * 3.11: Faster Code Execution
  * 3.12: Better f-strings
  * 3.13: JIT Compiler

Queremos jugar con varias versiones, pero no debemos cambiar la versión en el Sistema Operativo

---
layout: image
image: './python_background.jpg'
---

# ¿Qué problema queremos resolver?

## Varias Versiones de Python

<br />

```bash
python3 --version
Python 3.11.7

which python3
/usr/local/bin/python3

ls -l /usr/local/bin/python*
lrwxr-xr-x  1 david.sol  admin  40 Jan  5 17:26 /usr/local/bin/python3 -> ../Cellar/python@3.11/3.11.7/bin/python3
lrwxr-xr-x  1 david.sol  admin  46 Nov  3 15:48 /usr/local/bin/python3.10 -> ../Cellar/python@3.10/3.10.13_1/bin/python3.10
lrwxr-xr-x  1 david.sol  admin  43 Jan  5 17:26 /usr/local/bin/python3.11 -> ../Cellar/python@3.11/3.11.7/bin/python3.11
lrwxr-xr-x@ 1 david.sol  admin  43 Dec 12 20:15 /usr/local/bin/python3.12 -> ../Cellar/python@3.12/3.12.1/bin/python3.12
lrwxr-xr-x  1 david.sol  admin  43 Nov  3 15:49 /usr/local/bin/python3.9 -> ../Cellar/python@3.9/3.9.18_1/bin/python3.9
```

---
layout: image
image: './python_background.jpg'
---

# ¿Qué problema queremos resolver?

## Varias Versiones de Python

<br />

### Linux <logos-tux />

```bash
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt update
sudo apt install python3.11
python3.11 --version
```

<br />

### MacOS <mdi-apple />

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew update
brew install python@3.11
python3.11 --version
```

---
layout: image
image: './python_background.jpg'
---

# ¿Qué problema queremos resolver?

## Versión de Librerias

```bash {1|9-30}
pip install jupyter
Collecting jupyter
  Using cached jupyter-1.0.0-py2.py3-none-any.whl (2.7 kB)
Collecting notebook (from jupyter)
  Using cached notebook-7.0.6-py3-none-any.whl.metadata (10 kB)
Collecting qtconsole (from jupyter)
  Using cached qtconsole-5.5.1-py3-none-any.whl.metadata (5.1 kB)
...
Successfully installed anyio-4.2.0 argon2-cffi-23.1.0 argon2-cffi-bindings-21.2.0 arrow-1.3.0 asttokens-2.4.1
async-lru-2.0.4 attrs-23.2.0 babel-2.14.0 beautifulsoup4-4.12.2 bleach-6.1.0 certifi-2023.11.17 cffi-1.16.0
charset-normalizer-3.3.2 comm-0.2.1 debugpy-1.8.0 decorator-5.1.1 defusedxml-0.7.1 executing-2.0.1
fastjsonschema-2.19.1 fqdn-1.5.1 idna-3.6 ipykernel-6.28.0 ipython-8.20.0 ipywidgets-8.1.1 isoduration-20.11.0
jedi-0.19.1 jinja2-3.1.3 json5-0.9.14 jsonpointer-2.4 jsonschema-4.20.0 jsonschema-specifications-2023.12.1
jupyter-1.0.0 jupyter-client-8.6.0 jupyter-console-6.6.3 jupyter-core-5.7.1 jupyter-events-0.9.0
jupyter-lsp-2.2.1 jupyter-server-2.12.4 jupyter-server-terminals-0.5.1 jupyterlab-4.0.10 jupyterlab-pygments-0.3.0
jupyterlab-server-2.25.2 jupyterlab-widgets-3.0.9 markupsafe-2.1.3 matplotlib-inline-0.1.6 mistune-3.0.2
nbclient-0.9.0 nbconvert-7.14.1 nbformat-5.9.2 nest-asyncio-1.5.9 notebook-7.0.6 notebook-shim-0.2.3
overrides-7.4.0 packaging-23.2 pandocfilters-1.5.0 parso-0.8.3 pexpect-4.9.0 platformdirs-4.1.0
prometheus-client-0.19.0 prompt-toolkit-3.0.43 psutil-5.9.7 ptyprocess-0.7.0 pure-eval-0.2.2 pycparser-2.21
pygments-2.17.2 python-dateutil-2.8.2 python-json-logger-2.0.7 pyyaml-6.0.1 pyzmq-25.1.2 qtconsole-5.5.1 qtpy-2.4.1
referencing-0.32.1 requests-2.31.0 rfc3339-validator-0.1.4 rfc3986-validator-0.1.1 rpds-py-0.17.1 send2trash-1.8.2
six-1.16.0 sniffio-1.3.0 soupsieve-2.5 stack-data-0.6.3 terminado-0.18.0 tinycss2-1.2.1 tornado-6.4 traitlets-5.14.1
types-python-dateutil-2.8.19.20240106 uri-template-1.3.0 urllib3-2.1.0 wcwidth-0.2.13 webcolors-1.13
webencodings-0.5.1 websocket-client-1.7.0 widgetsnbextension-4.0.9
```

---
layout: two-cols-header
background: './python_background.jpg'
---

# ¿Qué problema queremos resolver?

## Versión de Librerias

¿Qué sucede si tenemos dos librerías que tienen dependencias incompatibles?

::left::

## Librería-A

<br />

* Librería-C (De versión 1 a versión 3)

::right::

## Librería-B

<br />

* Librería-C (De versión 4 en adelante)

---
layout: image-right
image: './tools.jpg'
---

# Versionado Semántico

<https://semver.org>

## Librería-X.Y.Z

<br />

### X - Versión Mayor

- Cambios Importantes (Breaking)

### Y - Versión Menor

- Nueva Funcionalidad

### Z - Parche

- Correcciones (Patch)

*Advertencia: Nunca es tan sencillo como eso*

---
layout: cover
background: './python_background.jpg'
---

# Soluciones

---
layout: two-cols-header
background: './python_background.jpg'
---

# venv

Forma estándar de crear ambientes virtuales.

<https://docs.python.org/3/library/venv.html>

::left::

#### Linux

```bash
# Create new environment
python3 -m venv .venv

# Activate an environment
.venv/bin/activate

# Deactivate an active environment
deactivate
```

::right::

#### Windows

```bash
# Create new environment
python3 -m venv venv

# Activate an environment
venv/Scripts/Activate.bat # (or .ps1)

# Deactivate an active environment
deactivate
```

---
layout: image
image: './python_background.jpg'
---

# Requirements file

<https://pip.pypa.io/en/stable/reference/requirements-file-format/>

```text
# It is possible to specify requirements as plain names.
pytest
pytest-cov
beautifulsoup4

# The syntax supported here is the same as that of requirement specifiers.
docopt == 0.6.1
requests >= 2.8.1, == 2.8.*

# It can include the has for a package, to make it very secure
pytz==2021.3 \
    --hash=sha256:3672058bc3453457b622aab7a1c3bfd5ab0bdae451512f6cf25f64ed37f5b87c \
    --hash=sha256:acad2d8b20a1af07d4e4c9d2e9285c5ed9104354062f275f3fcd88dcef4f1326
```

Se pueden instalar todos juntos

```bash
pip install -r requirements.txt
```

---
layout: image
image: './python_background.jpg'
---

# poetry

<https://python-poetry.org>

``` bash
# Create directory structure and pyproject.toml
poetry new <project_name>

# Install package from pyproject.toml
poetry install

# Add dependency
poetry add <package_name> [--group dev]

# Display all dependencies
poetry show --tree

# Activate virtual env
poetry shell

# Run script within virtual env
poetry run python <script_name.py>
```

---
layout: image
image: './python_background.jpg'
---

# pyproject.toml

Archivo para definir los metadatos de un proyecto, definido en [PEP518](https://peps.python.org/pep-0518/).

```ini
[tool.poetry]
name = "poetry-demo"
version = "0.1.0"
description = ""
authors = ["Sébastien Eustace <sebastien@eustace.io>"]
readme = "README.md"
packages = [{include = "poetry_demo"}]

[tool.poetry.dependencies]
python = "^3.7"

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
```

---
layout: image
image: './python_background.jpg'
---

# conda

<https://www.anaconda.com>

Hay dos versiones:

- Anaconda: Miles de paquetes listos para hacer Data Engineering, AI/ML, etc.
- Miniconda: Solo el administrador de paquetes **conda**

¡Tiene costo!

![Anaconda Logo](/anaconda.png)

---
layout: image
image: './python_background.jpg'
---

# pyenv

Permite instalar y admininstrar varias versiones de Python en el sistema.

<https://github.com/pyenv/pyenv>

```bash
# Install specific Python version
pyenv install 3.10.4

# Switch between Python versions
pyenv shell <version> # select version just for current shell session
pyenv local <version> # automatically select version whenever you are in the current directory 
pyenv global <version> # select version globally for your user account
```

---
layout: image-left
image: './colors.jpg'
---

# Otros

* [pipenv](https://github.com/pypa/pipenv)
* [pdm](https://pdm-project.org/latest/)
* [rye](https://github.com/mitsuhiko/rye)
* [PyFlow](https://github.com/David-OConnor/pyflow)
* [Hatch](https://hatch.pypa.io/1.9/)
* [Tox](https://tox.wiki/en/4.12.0/)

---
layout: image
image: './python_background.jpg'
---

# Resumen

- **venv** es la forma estándar, todos la deben conocer, y está disponible universalmente. Es mejor para material educativo y para código abierto.
- **Poetry** cuendo se está haciendo desarrollo de aplicaciones, especialmente si la reproducibilidad es importante, el grupo de trabajo está restringido, y si se publican los paquetes.
- **Anaconda** cuando solo se quiere hacer Inteligencia Artificial y Machine Learning, sin hacer desarrollo en Python.
- Las demás herramientas según las preferencias, reglas de los equipos o empresas, o casos específicos.
---
layout: image
image: './python_background.jpg'
---

# Para más información

<br />

[An unbiased evaluation of environment management and packaging tools](https://alpopkes.com/posts/python/packaging_tools/)

\- Anna-Lena Popkes

<style>
  img {
  display: block;
  margin-left: auto;
  margin-right: auto;
}
</style>

![Venn Diagram of Python Tools](/venn_diagram_small.png)

---
layout: cover
background: './questions.jpg'
---

# Preguntas y Respuestas

---
layout: image
image: './python_background.jpg'
---

# Python CDMX

<logos-telegram /> &nbsp;&nbsp; Telegram: &nbsp;&nbsp; <https://t.me/pythonCDMX><br />

<logos-twitter /> &nbsp;&nbsp; Twitter: &nbsp;&nbsp; <https://twitter.com/PythonMexico><br />

<logos-google-meet /> &nbsp;&nbsp; Meetup: &nbsp;&nbsp; <https://www.meetup.com/es-ES/python-mexico/><br />

<logos-github-icon class="invert" /> &nbsp;&nbsp; Github: &nbsp;&nbsp; <https://github.com/PythonMexico/python-cdmx-charlas/issues><br />

<style>
img {
  display: block;
  margin-left: auto;
  margin-right: auto;
}

.invert{
  filter: invert(0%);
}
</style>

![Logo Python CDMX](/PythonCDMXLogo.png)

---
layout: cover
background: './python_background.jpg'
---

# Gracias
