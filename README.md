# Python CDMX Meetup Enero 2024

### Ambientes Virtuales con venv, pyenv, conda y poetry

- David Sol, SRE @ Wizeline

[Presentación para el navegador.](https://pythoncdmx20240116-soldavid-b516215550e1b0bf230d89e7c8d7a911c1e.gitlab.io/1)

[Presentación como PDF.](https://pythoncdmx20240116-soldavid-b516215550e1b0bf230d89e7c8d7a911c1e.gitlab.io/PythonCDMX20240116.pdf)

[Liga al Meetup en Python CDMX.](https://www.meetup.com/python-mexico/events/298328457)

Presentación hecha con [Slidev](https://sli.dev/).

![Meetup Hero](static/hero.jpg)
